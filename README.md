# PhotonOS `icosa_sr` Manifest

## Getting Started

To get started with Android/Maru OS, you'll need to get familiar with
[Repo](https://source.android.com/source/using-repo.html) and [Version Control
with Git](https://source.android.com/source/version-control.html).

To initialize your local repository using the PhotonOS trees, use a command like this:

    repo init -u https://gitlab.com/photonos/manifest -b photon-0.1 --no-clone-bundle --depth=1

Then to sync up:

    repo sync

## Patching

* Repopick topic `icosa-bt-lineage-17.1`
* Repopick commits `302554`, `286019`, `270702`, and `299588` off Lineage Gerrit
* Apply all patches in `patches/` to their respective directories based on the format `path_to_patch-description.patch`

## Roadmap

* Get Maru OS working (see progress [here](https://gitlab.com/switchroot-maru-os/manifest))
* Create working photon vendor repo
* Implement custom UI
* Add other device support

## Important details

Uses customized frameworks/base, frameworks/native, and build/make repos with built in Nvidia enhancements support

Device support:
* Nintendo Switch (`photon_icosa_sr`)
* More to come!

## Contributing

See the [main Maru OS repository](https://github.com/maruos/maruos) for Maru OS contributing information.

See the [LineageOS charter repository](https://github.com/LineageOS/charter) for LineageOS contributing information.

Feel free to submit Merge Requests for code review.

## Licensing

[Apache 2.0](LICENSE)
